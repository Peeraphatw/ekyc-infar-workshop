import axios , {AxiosInstance} from "axios";


const Axios: AxiosInstance = axios.create({
    baseURL: "https://jsonplaceholder.typicode.com",
    timeout: 5000,
    headers : {
        'Content-Type': 'application/json',
    }
})

export default Axios