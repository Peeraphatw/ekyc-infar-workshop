"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// src/app.ts
const express_1 = __importDefault(require("express"));
const axios_instance_1 = __importDefault(require("./axios.instance"));
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const app = (0, express_1.default)();
const port = parseInt(process.env.APP_PORT || '8080');
app.use((0, cors_1.default)());
app.use((0, morgan_1.default)("common"));
app.get('/', (req, res) => {
    res.json({
        message: 'api service is available',
    });
});
app.get('/data', (req, res) => {
    try {
        axios_instance_1.default.get('/posts').then((response) => res.status(200).json(response.data));
    }
    catch (error) {
        res.status(500).json({
            error: true,
        });
    }
});
app.listen(port, () => console.log(`Application is running on port ${port}`));
