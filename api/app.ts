// src/app.ts
import express, { Express, Request, Response, response } from 'express'
import Axios from './axios.instance'
import cors from 'cors'
import morgan from 'morgan'

const app: Express = express()

const port: number = parseInt(process.env.APP_PORT || '8080')
app.use(cors())
app.use(morgan("common"))
app.get('/', (req: Request, res: Response) => {
  res.json({
    message: 'api service is available',
  })
})

app.get('/data',(req: Request,res: Response) => {
    try{
        Axios.get('/posts').then((response) => res.status(200).json(response.data))
    }catch(error){
        res.status(500).json({
            error: true,
        })
    }
    
})

app.listen(port, () => console.log(`Application is running on port ${port}`))
