import axios , {AxiosInstance} from "axios";


const Axios : AxiosInstance = axios.create({
    baseURL : process.env.API_ENDPOINT || "http://localhost:8080",
    timeout: 5000,
    headers : {
        'Content-Type': 'application/json',
    }
}) 

export default Axios

