import {useEffect,useState} from "react";
import "./App.css";
import Axios from "./utils/axios.instance";
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from "@chakra-ui/react";

interface Post {
    userId: number
    id: number
    title: string
    body: string
}

function App() {
  const [Posts,SetPosts] = useState<Post[]>([{
    userId : 0,
    id : 0,
    title : "",
    body : ""
  }])
  useEffect(() => {
    Axios.get('/data').then(response => SetPosts(response.data))
  },[])
  return (
    <div className="App">
      <TableContainer>
        <Table variant="striped" colorScheme="teal">
          <TableCaption>Imperial to metric conversion factors</TableCaption>
          <Thead>
            <Tr>
              <Th>Id</Th>
              <Th>Title</Th>
              {/* <Th>Body</Th>
              <Th>UserId</Th> */}
            </Tr>
          </Thead>
          <Tbody>
            {
              Posts.map((Post) => (<Tr>
                <Td>{Post.id}</Td>
                <Td>{Post.title}</Td>
                {/* <Td>{Post.body}</Td>
                <Td>{Post.userId}</Td> */}
                </Tr>
                ))
            }
            <Tr>
              <Td>inches</Td>
              <Td>millimetres (mm)</Td>
              <Td isNumeric>25.4</Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default App;
